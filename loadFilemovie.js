function loadAllStudents() {
    var responseData = null;
    var data = fetch('https://dv-excercise-backend.appspot.com/movies')
        .then((response) => {
            console.log(response)
            return response.json()
        }).then((filmDetail) => {
            responseData = filmDetail
            var resultElement = document.getElementById('result')
            resultElement.innerHTML = JSON.stringify(filmDetail, null, 6)
        })
}

async function loadAllStudentAsync() {
    let response = await fetch('https://dv-excercise-backend.appspot.com/movies')
    let data = await response.json()
    var resultElement = document.getElementById('result')
    resultElement.innerHTML = JSON.stringify(data, null, 2)
    return data
}

function createResultTable(data) {
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    var tableRawNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRawNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((filmDetail) => {
        console.log(filmDetail)
        for (let i = 0; i < filmDetail.length; i++) {
            var currentData = filmDetail[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFirstColumnNode = document.createElement('th')
            dataFirstColumnNode.setAttribute('scope', 'row')
            dataFirstColumnNode.innerText = i+1
            dataRow.appendChild(dataFirstColumnNode)

            var columeNode = null;

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['name']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['synopsis']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['imageUrl'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)

        }
    })
}

async function loadmove(

) {
    let movieId = document.getElementById('queryId').value
    console.log(movieId)
    if (movieId != '' && movieId != null) {
        let response = await fetch('https://dv-excercise-backend.appspot.com/movies/' + movieId)
        let data = await response.json()
        return data;
    }
}
function createResultSearchTable(data) {

    let resultElementTable = document.getElementById('serach')
    let checkResultClear = document.getElementById('idDiv')

    let divResult = document.createElement('div')
    divResult.setAttribute('id','idDiv')
    divResult.setAttribute('style','text-align: center;position:center;')
    resultElementTable.appendChild(divResult)
    // resultElementTable.innerHTML=""

    if (checkResultClear != null) {
        resultElementTable.removeChild(checkResultClear)
    }
    data.then(filmDetail => {
        for(let i=0;i<filmDetail.length;i++){
            let showDetail=filmDetail[i]
            console.log(showDetail)
            let profile = document.createElement('p')
            profile.innerHTML = 'Movie'
            profile.setAttribute('style', 'text-align: center;font-weight: bold;')
            divResult.appendChild(profile)
            let imageNode = document.createElement('img')
            imageNode.setAttribute('src', showDetail['imageUrl'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            divResult.appendChild(imageNode)
            let name = document.createElement('p')
            name.innerText = 'ชื่อหนัง---' + showDetail['name']
            divResult.appendChild(name)
    
            let synopsis = document.createElement('p')
            synopsis.innerText = 'เรื่องย่อ--- '+ showDetail['synopsis']
            synopsis.setAttribute('style', 'margin:0px 450px')
            divResult.appendChild(synopsis)
    
        }
    })

}
