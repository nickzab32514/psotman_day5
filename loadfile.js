function loadAllStudents() {
    var responseData = null;
    var data = fetch('https://dv-student-backend-2019.appspot.com/students')
        .then((response) => {
            console.log(response)
            return response.json()
        }).then((json) => {
            responseData = json
            var resultElement = document.getElementById('result')
            resultElement.innerHTML = JSON.stringify(json, null, 6)
        })
}

async function loadAllStudentAsync() {
    let response = await fetch('https://dv-student-backend-2019.appspot.com/students')
    let data = await response.json()
    var resultElement = document.getElementById('result')
    resultElement.innerHTML = JSON.stringify(data, null, 2)
    return data
}

function createResultTable(data) {
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    var tableRawNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRawNode)
    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'studentID'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'surname'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'gpa'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((json) => {
        console.log(json)
        for (let i = 0; i < json.length; i++) {
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFirstColumnNode = document.createElement('th')
            dataFirstColumnNode.setAttribute('scope', 'row')
            dataFirstColumnNode.innerText = currentData['id']
            dataRow.appendChild(dataFirstColumnNode)

            var columeNode = null;
            columeNode = document.createElement('td')
            columeNode.innerText = currentData['studentId']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['name']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['surname']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['gpa']
            dataRow.appendChild(columeNode)


            columeNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['image'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)

        }
    })
}

async function loadOneStudent(

) {
    let studentId = document.getElementById('queryId').value
    console.log(studentId)
    if (studentId != '' && studentId != null) {
        let response = await fetch('https://dv-student-backend-2019.appspot.com/students/' + studentId)
        let data = await response.json()
        return data;
    }
}
function createResultSearchTable(data) {

    var resultElementTable = document.getElementById('serach')
    var checkResultClear = document.getElementById('idDiv')

    let divResult = document.createElement('div')
    divResult.setAttribute('id','idDiv')
    resultElementTable.appendChild(divResult)
    // resultElementTable.innerHTML=""

    if (checkResultClear != null) {
        resultElementTable.removeChild(checkResultClear)
    }
    data.then(json => {

        let profile = document.createElement('p')
        profile.innerHTML = 'Profile'
        profile.setAttribute('style', 'text-align: center;font-weight: bold;')
        divResult.appendChild(profile)

        let imageNode = document.createElement('img')
        imageNode.setAttribute('src', json['image'])
        imageNode.style.width = '200px'
        imageNode.style.height = '200px'
        divResult.appendChild(imageNode)

        let stu = document.createElement('p')
        stu.innerText = 'ID=' + json['id']
        // stu.setAttribute('id', 'idResult')
        divResult.appendChild(stu)

        let name = document.createElement('p')
        name.innerText = 'name=' + json['name']
        divResult.appendChild(name)

        let surname = document.createElement('p')
        surname.innerText = 'surname=' + json['surname']
        divResult.appendChild(surname)

        let gpa = document.createElement('p')
        gpa.innerText = 'gpa=' + json['gpa']
        divResult.appendChild(gpa)

        let penAmount = document.createElement('p')
        penAmount.innerText = 'penAmount=' + json['penAmount']
        divResult.appendChild(penAmount)

        let description = document.createElement('p')
        description.innerText = 'description=' + json['description']
        divResult.appendChild(description)


    })

}
// async function loadOneStudent() {
//     let studentId = document.getElementById('queryId').value
//     console.log(studentId)
//     if (studentId != '' && studentId != null) {
//         let data = await fetch('https://dv-student-backend-2019.appspot.com/students/' + studentId)
//            let arr=[data]
        // return arr;
//     }
// }